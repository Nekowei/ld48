using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.ld48
{
    public class BodyController : MonoBehaviour
    {

        public Action<GameObject, bool> onEnemyColission;

        void OnCollisionEnter2D(Collision2D collision)
        {
            Debug.Log("p");
            if (collision.gameObject.tag == "enemy")
            {
                onEnemyColission.Invoke(collision.gameObject, true);
            }
        }
    }
}
