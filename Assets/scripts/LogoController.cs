using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Nekowei.ld48
{
    public class LogoController : MonoBehaviour
    {
        void Update()
        {
            if (Mouse.current.IsPressed()
                || Keyboard.current.anyKey.IsPressed())
            {
                SceneManager.LoadScene("main");
            }
            Camera.main.orthographicSize = Mathf.Lerp(
                Camera.main.orthographicSize, 5, 0.5f * Time.deltaTime);
        }
    }
}
