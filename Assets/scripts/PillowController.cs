using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.ld48
{
    public class PillowController : MonoBehaviour
    {

        public bool isEquiped = false;
        public bool isEnabled = false;
        public float speed;
        public float radius;
        public Action<GameObject, bool> onEnemyColission;

        void Update()
        {
            if (isEquiped && isEnabled)
            {
                Vector3 youPos = transform.parent.position;
                float sin = Mathf.Sin(Time.realtimeSinceStartup * speed);
                float cos = Mathf.Cos(Time.realtimeSinceStartup * speed);
                transform.position = Vector3.MoveTowards(
                    transform.position,
                    new Vector3(youPos.x + sin * radius, youPos.y + cos * radius, youPos.z),
                    speed * Time.deltaTime);
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            Debug.Log("p");
            if (collision.gameObject.tag == "enemy")
            {
                onEnemyColission.Invoke(collision.gameObject, false);
            }
        }

    }
}
