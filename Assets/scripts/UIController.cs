using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nekowei.ld48 
{
    public class UIController : MonoBehaviour
    {
        public int gameSpeed = 1; // second interval

        public GameObject youGO;
        private YouController you;

        public GameObject[] buttons;
        public int[] prices;
        public int[] count;

        public GameObject cokeOne;
        public GameObject chickenOne;

        private DateTime time = DateTime.Parse("08:00");
        public int income = 0;

        private float lastTime;
        private const string format = "HH:mm";

        private GameObject sleepButton;
        private GameObject sellPanel;
        private RectTransform tiredProgress;
        private RectTransform tiredProgressMax;
        private float progressWidth;
        private float progressAnimeSpeed = 0.01f;

        private Transform cokePanel;
        private Transform chickenPanel;

        public GameObject dreamCoke;
        public GameObject dreamChicken;
        public GameObject dreamPillow;
        public GameObject dreamTeddy;
        private List<GameObject> enemyList;
        public float enemySpeed;

        public void Start()
        {
            lastTime = Time.realtimeSinceStartup;
            sleepButton = transform.Find("Sleep").gameObject;
            sleepButton.SetActive(false);
            sellPanel = transform.Find("SellPanel").gameObject;
            sellPanel.SetActive(true);

            you = youGO.GetComponent<YouController>();
            you.onWakeUp = OnWakeUp;
            you.body.GetComponent<BodyController>().onEnemyColission = enemyColission;
            you.thePillow.GetComponent<PillowController>().onEnemyColission = enemyColission;
            you.theTeddy.GetComponent<TeddyController>().onEnemyColission = enemyColission;

            tiredProgress = transform.Find("TiredProgress").GetComponent<RectTransform>();
            tiredProgressMax = transform.Find("TiredProgressMax").GetComponent<RectTransform>();
            progressWidth = tiredProgress.rect.width;
            for (int i = 0; i < prices.Length; i++)
            {
                buttons[i].transform.Find("Text").GetComponent<Text>().text = "$" + prices[i];
            }
            enemyList = new List<GameObject>();

            cokePanel = transform.Find("CokePanel");
            chickenPanel = transform.Find("ChickenPanel");

        }

        public void Update()
        {
            // update data for a certain interval
            if (Time.realtimeSinceStartup - lastTime > gameSpeed)
            {

                float restValue = you.updateRestValue();
                if (you.isAsleep)
                {
                    int r = UnityEngine.Random.Range(0, 100);
                    if (you.transform.position.y < 0 && r > 60)
                    {
                        initEnemy(dreamCoke);
                    }
                    if (you.transform.position.y < -2 && r > 50)
                    {
                        initEnemy(dreamChicken);
                    }
                    if (you.transform.position.y < -4 && r > 40)
                    {
                        initEnemy(dreamPillow);
                    }
                    if (you.transform.position.y < -6 && r > 30)
                    {
                        initEnemy(dreamTeddy);
                    }
                }
                else
                {
                    time = time.AddMinutes(60);
                    transform.Find("TimeText").GetComponent<Text>().text = time.ToString(format);

                    updateIncome(restValue);

                    if (time.ToString(format).CompareTo("20:00") >= 0)
                    {
                        sleepButton.SetActive(true);
                    }
                }

                progressWidth = tiredProgressMax.rect.width * (100 - restValue) / 100;

                lastTime = Time.realtimeSinceStartup;
            }

            // upadte UI everytime
            tiredProgress.sizeDelta = new Vector2(
                Mathf.Lerp(tiredProgress.rect.width, progressWidth, progressAnimeSpeed), 
                tiredProgress.rect.height);

            foreach(GameObject enemy in enemyList)
            {
                if (enemy.activeSelf)
                {
                    Vector3 target = you.transform.position + new Vector3(UnityEngine.Random.Range(0, 1), UnityEngine.Random.Range(0, 1), 0);
                    enemy.transform.position = Vector3.MoveTowards(
                        enemy.transform.position, target, enemySpeed * Time.deltaTime);
                }
            }
        }

        private void initEnemy(GameObject prefab)
        {
            if (!you.gameover.activeSelf)
            {
                GameObject enemy = Instantiate(prefab);
                enemyList.Add(enemy);
                enemy.transform.position = 
                    you.transform.position - new Vector3(UnityEngine.Random.Range(0, 2), 2, 0);
            }
        }

        private void updateIncome(float restValue)
        {
            if (restValue > 50)
            {
                income += Mathf.RoundToInt(restValue / 2);
            }
            else
            {
                income += Mathf.RoundToInt(restValue / 10);
            }
            transform.Find("IncomeText").GetComponent<Text>().text = "$" + income;
            //buttons[4].GetComponent<Image>().color = income < 999 ? Color.gray : Color.white;
            buttons[3].GetComponent<Image>().color = income < 250 ? Color.gray : Color.white;
            buttons[2].GetComponent<Image>().color = income < 100 ? Color.gray : Color.white;
            buttons[1].GetComponent<Image>().color = income < 5 ? Color.gray : Color.white;
            buttons[0].GetComponent<Image>().color = income < 1 ? Color.gray : Color.white;
        }

        public void Sleep()
        {
            you.sleep();
            sleepButton.SetActive(false);
            sellPanel.SetActive(false);
            transform.Find("TimeText").gameObject.SetActive(false);
            transform.Find("IncomeText").gameObject.SetActive(false);
        }

        public void Buy1()
        {
            if (count[0] < 5)
            {
                buy(0);
                RectTransform rt = Instantiate(cokeOne).GetComponent<RectTransform>();
                rt.SetParent(cokePanel);
                rt.localPosition = new Vector3(0, -50 * (count[0] - 1), 0);
            }
        }

        public void Buy2()
        {
            if (count[1] < 5)
            {
                buy(1);
                RectTransform rt = Instantiate(chickenOne).GetComponent<RectTransform>();
                rt.SetParent(chickenPanel);
                rt.localPosition = new Vector3(0, -50 * (count[1] - 1), 0);
            }
        }

        public void Buy3()
        {
            if (income > prices[2])
            {
                buy(2);
                you.thePillow.gameObject.SetActive(true);
                you.thePillow.GetComponent<PillowController>().isEquiped = true;
            }
            else
            {
                if (count[2] > 0)
                {
                    income -= prices[2];
                    transform.Find("IncomeText").GetComponent<Text>().text = "$" + income;
                    you.thePillow.GetComponent<PillowController>().speed++;
                }
            }
        }

        public void Buy4()
        {
            if (income > prices[3])
            {
                buy(3);
                you.theTeddy.gameObject.SetActive(true);
                you.theTeddy.GetComponent<TeddyController>().isEquiped = true;
            }
            else
            {
                if (count[3] > 0)
                {
                    income -= prices[3];
                    transform.Find("IncomeText").GetComponent<Text>().text = "$" + income;
                    you.theTeddy.GetComponent<TeddyController>().speed++;
                }
            }
        }

        //public void Buy5()
        //{
        //    buy(4);
        //}

        private void buy(int i)
        {
            if (income > prices[i])
            {
                income -= prices[i];
                transform.Find("IncomeText").GetComponent<Text>().text = "$" + income;
                count[i]++;
            }
        }

        public void OnWakeUp()
        {
            time = DateTime.Parse("08:00");
            foreach(GameObject e in enemyList)
            {
                Destroy(e);
            }
            enemyList.Clear();
            sellPanel.SetActive(true);
            transform.Find("TimeText").gameObject.SetActive(true);
            transform.Find("IncomeText").gameObject.SetActive(true);
            for (int i = 0; i < cokePanel.transform.childCount; i++)
            {
                Destroy(cokePanel.transform.GetChild(i).gameObject);
            }
            count[0] = 0;
            for (int i = 0; i < chickenPanel.transform.childCount; i++)
            {
                Destroy(chickenPanel.transform.GetChild(i).gameObject);
            }
            count[1] = 0;
        }

        public void enemyColission(GameObject enemy, bool cost)
        {
            enemy.transform.parent.gameObject.SetActive(false);
            if (enemy.name == "dreamCoke" && count[0] > 0)
            {
                if (cost)
                {
                    count[0]--;
                    Destroy(cokePanel.transform.GetChild(cokePanel.transform.childCount - 1).gameObject);
                }
                return;
            }
            if (enemy.name == "dreamChicken" && count[1] > 0)
            {
                if (cost)
                {
                    count[1]--;
                    Destroy(chickenPanel.transform.GetChild(chickenPanel.transform.childCount - 1).gameObject);
                }
                return;
            }
            if (cost)
            {
                you.rise();
            }
        }

    }
}
