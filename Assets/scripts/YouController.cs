using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Nekowei.ld48
{
    [RequireComponent(typeof(AudioSource))]
    public class YouController : MonoBehaviour
    {
        [Range(0, 100)]
        public float restValue; // range(0, 100)
        public int restSpeed = 3;
        public float fallSpeed = -1;
        public float riseSpeed = 2;
        public float zoomOutSpeed = 0.05f;
        public float zoomInSpeed = 0.1f;

        public float minSize = 0.6f;
        public float maxSize = 1.1f;

        private float startY = 0;
        private bool isFall = true;

        public bool isAsleep = false;

        public Sprite[] youSit;
        public Sprite youSleep;
        public Sprite[] youHard;
        private SpriteRenderer sr;
        private SpriteRenderer srHard;
        private int spriteCount = 0;

        private AudioSource audioSrc;
        public AudioClip lullaby;
        public AudioClip workHard;
        public AudioClip workTired;

        public Transform chairPosition;
        public Transform bedPosition;
        public Transform bedPositionEnd;
        public Transform pillowPosition;
        public Transform teddyPosition;
        public Transform thePillow;
        public Transform theTeddy;
        public Transform body;

        public Action onWakeUp;

        public GameObject gameover;

        void Awake()
        {
            sr = body.GetComponent<SpriteRenderer>();
            srHard = transform.Find("hard").GetComponent<SpriteRenderer>();
            audioSrc = GetComponent<AudioSource>();
            thePillow.gameObject.SetActive(false);
            theTeddy.gameObject.SetActive(false);
            gameover.SetActive(false);
        }

        void Update()
        {
            srHard.sprite = null;
            if (isAsleep)
            {
                updateSleep();
            }
            else
            {
                updateAwake();
            }
        }

        private void updateAwake()
        {
            if (restValue == 0)
            {
                return; // too tired to do anything!
            }
            float interval;
            if (restValue > 75)
            {
                interval = 2;
            }
            else if (75 >= restValue && restValue > 50)
            {
                interval = 4;
            }
            else if (50 >= restValue && restValue > 25)
            {
                interval = 6;
            }
            else
            {
                interval = 8;
            }
            spriteCount = Time.realtimeSinceStartup % interval > interval / 2 ? 1 : 0;
            if (restValue > 50)
            {
                if (!audioSrc.isPlaying || audioSrc.clip.Equals(workTired))
                {
                    audioSrc.clip = workHard;
                    audioSrc.volume = 0.5f;
                    audioSrc.Play();
                }
                sr.sprite = youSit[spriteCount];
                srHard.sprite = youHard[spriteCount];
            } 
            else
            {
                if (!audioSrc.isPlaying || audioSrc.clip.Equals(workHard))
                {
                    audioSrc.clip = workTired;
                    audioSrc.volume = 0.5f;
                    audioSrc.Play();
                }
                sr.sprite = youSit[spriteCount + 2];
            }
            
        }

        private void updateSleep()
        {
            float speed;
            if (!isFall && transform.position.y > startY)
            {
                wakeUp();
            }
            if (isFall)
            {
                speed = fallSpeed;
            }
            else
            {
                speed = riseSpeed;
            }
            if (isFall && transform.position.y < bedPositionEnd.position.y)
            {
                // game over!
                speed = 0f;
                gameover.SetActive(true);
            }
            var tp = transform.position;
            transform.position = new Vector3(tp.x, tp.y + speed * Time.deltaTime, tp.z);
            
            var cp = Camera.main.transform.position;
            Camera.main.transform.position = new Vector3(cp.x, cp.y + speed * Time.deltaTime, cp.z);
            
            float targetSize = isFall ? maxSize : minSize;
            float zoomSpeed = isFall ? zoomOutSpeed : zoomInSpeed;
            Camera.main.orthographicSize = Mathf.Lerp(
                Camera.main.orthographicSize, targetSize,  zoomSpeed * Time.deltaTime);

            if (gameover.activeSelf && gameover.transform.localScale.y < 12)
            {
                gameover.transform.localScale += Vector3.one;
            }

        }

        public void sleep()
        {
            isAsleep = true;
            transform.position = bedPosition.position;
            sr.sprite = youSleep;
            startY = transform.position.y;
            audioSrc.clip = lullaby;
            audioSrc.volume = 1f;
            audioSrc.Play();
            thePillow.GetComponent<PillowController>().isEnabled = true;
            theTeddy.GetComponent<TeddyController>().isEnabled = true;
        }

        public void rise()
        {
            isFall = false;
            audioSrc.Stop();
        }

        public void wakeUp()
        {
            isFall = true;
            isAsleep = false;
            transform.position = chairPosition.position;
            thePillow.position = pillowPosition.position;
            thePillow.GetComponent<PillowController>().isEnabled = false;
            theTeddy.position = teddyPosition.position;
            theTeddy.GetComponent<TeddyController>().isEnabled = false;
            theTeddy.GetComponent<TeddyController>().reset();
            onWakeUp.Invoke();
        }

        public float updateRestValue()
        {
            if (isAsleep)
            {
                restValue += restSpeed;
                if (restValue >= 100)
                {
                    restValue = 100;
                }
            }
            else
            {
                restValue -= restSpeed;
                if (restValue <= 0)
                {
                    restValue = 0;
                }
            }
            return restValue;
        }


    }
}

