using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.ld48 {
    public class TeddyController : MonoBehaviour
    {

        public bool isEquiped = false;
        public bool isEnabled = false;
        public float speed;
        public Action<GameObject, bool> onEnemyColission;

        private bool inBattle = false;

        public Sprite normal;
        public Sprite battle;

        private float lastTime;
        public float switchTime;

        private Vector3 targetPos;

        void Start()
        {
            targetPos = transform.parent.position;
        }

        void Update()
        {
            if (isEquiped && isEnabled)
            {
                if (Time.realtimeSinceStartup - lastTime > switchTime)
                {
                    inBattle = !inBattle;
                    lastTime = Time.realtimeSinceStartup;
                    Vector3 youPos = transform.parent.position;
                    float offsetX = UnityEngine.Random.Range(-1, 1);
                    float offsetY = UnityEngine.Random.Range(-1, 0);
                    targetPos = new Vector3(youPos.x + offsetX, youPos.y + offsetY, youPos.z);
                }
                if (inBattle)
                {
                    GetComponent<SpriteRenderer>().sprite = battle;
                } 
                else
                {
                    GetComponent<SpriteRenderer>().sprite = normal;
                    transform.position = Vector3.MoveTowards(
                        transform.position,
                        targetPos,
                        speed * Time.deltaTime);
                }
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            Debug.Log("t");
            if (collision.gameObject.tag == "enemy")
            {
                onEnemyColission.Invoke(collision.gameObject, false);
            }
        }

        public void reset()
        {
            GetComponent<SpriteRenderer>().sprite = normal;
        }

    }
}
